from math import floor, sqrt
from os import path, remove
from random import randint
from statistics import mean, median
from time import perf_counter_ns
from xlsxwriter import Workbook

# lower and upper bounds for random numbers
LOWER = 1
UPPER = 1000

# The more basic brute algorithm. It starts checking gcds
# at 1 until b, where b is the smaller of the two numbers.
def gcdBruteV1(pair=(0,0)):
    a, b = pair

    # ensure that b is less than a.
    if b > a:
        a, b = b, a
    
    gcd = 1

    # check each number up to the lower number b 
    for i in range(1, b + 1):
        if a % i == 0 and b % i == 0:
            gcd = i

    # return highest found divisor
    return gcd

# A slightly less naive brute force algorithm. It will 
# only check until the square root of b, where b is 
# the smaller number
def gcdBruteV2(pair=(0,0)):
    a, b = pair

    # ensure b is the smaller number
    if b > a:
        a, b = b, a
    
    i = 1
    gcd = 0

    # checking if i * i is less than b is the same as if
    # checking that i is less than sqrt of b.
    while i * i <= b:
        # if the number divides into b, check if i or b / i
        # divide into a. Set the gcd as either i or b / i if
        # they are higher than the current gcd.
        if b % i == 0:
            if a % i == 0:
                gcd = floor(max(gcd, i))
            if a % (b / i) == 0:
                gcd = floor(max(gcd, b / i))
        i += 1

    return gcd

# A non naive algorithm that takes the modulus of a and b
# then replaces a with b and b with the remainder until
# the remainder is 0. The answer is then the value for a.
def gcdEuclid(pair=(0,0)):
    a, b = pair
    remainder = -1
    while remainder != 0:
        remainder = a % b
        a = b
        b = remainder
    return a

# this function takes in the pairs and one of the functions to
# find the gcds. It returns the gcds and the times for each calculation
# in two separate lists
def runCalculations(pairs, callback):
    gcds = []
    times = []
    
    for pair in pairs:
        # get the gcd and time the calculatino
        time1 = perf_counter_ns()
        gcd = callback(pair)
        time2 = perf_counter_ns()
        
        # next, add the gcd and time to their respective lists.
        gcds.append(gcd)
        times.append(time2 - time1)

    return gcds, times

# Will create an excel spreadsheet with the results from a 
# run of one of the algorithms.
def writeResults(worksheet, pairs, gcds, times):
    worksheet.write(0, 0, 'Number One')
    worksheet.write(0, 1, 'Number Two')
    worksheet.write(0, 2, 'GCD')
    worksheet.write(0, 3, 'Time Spent (microseconds)')

    row = 1

    # write in each row with a pair, their gcd, and the time it took
    for pair, gcd, time in zip(pairs, gcds, times):
        a, b = pair
    
        worksheet.write(row, 0, a)
        worksheet.write(row, 1, b)
        worksheet.write(row, 2, gcd)
        worksheet.write(row, 3, time)

        row += 1

# Similar to writeResults() but it creates an excel spreadsheet
# with some stats about the run of the algorithm.
def writeStats(worksheet, times):
    worksheet.write(0, 0, 'Statistics')
    worksheet.write(0, 1, 'Microseconds')
    worksheet.write(1, 0, 'Maximum Time')
    worksheet.write(2, 0, 'Minimum Time')
    worksheet.write(3, 0, 'Average Time')
    worksheet.write(4, 0, 'Median Time')

    worksheet.write(1, 1, max(times))
    worksheet.write(2, 1, min(times))
    worksheet.write(3, 1, round(mean(times), 3))
    worksheet.write(4, 1, median(times))
    
def writeStatsAndResults(name, pairs, gcds, times):
    # Delete the excel sheets if they already exists.
    if path.exists(name + '_Statistics.xlsx'):
        remove(name + '_Statistics.xlsx')
    if path.exists(name + '_Results.xlsx'):
        remove(name + '_Results.xlsx')

    # write to the stats excel spreadsheet
    statsWorkbook = Workbook(name + '_Statistics.xlsx')
    statsWorksheet = statsWorkbook.add_worksheet()
    writeStats(statsWorksheet, times)
    statsWorkbook.close()

    # write to the results excel spreadsheet
    resultsWorkbook = Workbook(name + '_Results.xlsx')
    resultsWorksheet = resultsWorkbook.add_worksheet()
    writeResults(resultsWorksheet, pairs, gcds, times)
    resultsWorkbook.close()

# Writes comparisons between the 3 algorithm time lists to a file
# named 'Conclusions.txt'
def writeConclusions(bruteV1Times, bruteV2Times, euclidTimes):
    fp = open('Conclusions.txt', 'w')

    # returns the number of times from list 1 that are lower than list2
    # along with the mean of the lower times.
    def getlowerNumMean(timeList1, timeList2):
        lowerTimes = []
        for time1, time2 in zip(timeList1, timeList2):
            if time1 < time2:
                lowerTimes.append(time1)
        return len(lowerTimes), mean(lowerTimes)

    v1v2NumTimes, v1v2Mean = getlowerNumMean(bruteV2Times, bruteV1Times)
    v1EuclidNumTimes, v1EuclidMean = getlowerNumMean(euclidTimes, bruteV1Times)
    v2EuclidNumTimes, v2EuclidMean = getlowerNumMean(euclidTimes, bruteV2Times)

    fp.write(
        ('Out of 100 pairs of integers, brute-force (v2) outperformed brute-force (v1) in {} pairs;\n'
        'and the average saved time for these {} pairs of integers was {} microseconds.\n\n').format(v1v2NumTimes, v1v2NumTimes, round(v1v2Mean, 3))
    )

    fp.write(
       ('Out of 100 pairs of integers, the original version of Euclid outperformed brute-force (v1) in {} pairs;\n'
        'and the average saved time for these {} pairs of integers was {} microseconds.\n\n').format(v1EuclidNumTimes, v1EuclidNumTimes, round(v1EuclidMean, 3))
    )

    fp.write(
        ('Out of 100 pairs of integers, the original version of Euclid outperformed brute-force (v2) in {} pairs;\n'
        'and the average saved time for these {} pairs of integers was {} milliseconds.\n').format(v2EuclidNumTimes, v2EuclidNumTimes, round(v2EuclidMean, 3))
    )

    fp.close()

# get our random numbers
pairs = []
for _ in range(100):
    pairs.append((randint(LOWER, UPPER), randint(LOWER, UPPER)))

# Run all our calculations
bruteV1Divisors, bruteV1Times = runCalculations(pairs, gcdBruteV1)
bruteV2Divisors, bruteV2Times = runCalculations(pairs, gcdBruteV2)
euclidDivisors, euclidTimes = runCalculations(pairs, gcdEuclid)

# We need to divide our times by 1000 because they are in nanoseconds 
# and microseconds would be more apropriate. 
bruteV1Times = [x / 1000 for x in bruteV1Times]
bruteV2Times = [x / 1000 for x in bruteV2Times]
euclidTimes = [x / 1000 for x in euclidTimes]

# Write to excel spreadsheets
writeStatsAndResults('Brute_Force_v1', pairs, bruteV1Divisors, bruteV1Times)
writeStatsAndResults('Brute_Force_v2', pairs, bruteV2Divisors, bruteV2Times)
writeStatsAndResults('Original_Euclid', pairs, euclidDivisors, euclidTimes)

writeConclusions(bruteV1Times, bruteV2Times, euclidTimes)